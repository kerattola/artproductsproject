<?php

use App\Fixtures\FileException;

class Autoload
{
    public static function load($className):bool
    {
        $fileName = str_replace('\\','/', $className);
        $fileName .= ".php";

        if (file_exists($fileName)) {
            include_once $fileName;
            return true;
        } else{
            throw new \App\Fixtures\FileException("File '$fileName' does not exist!");
        }

    }
}

spl_autoload_register("Autoload::load");

