<?php

namespace App\Code\Api;

use App\Code\Products\PaintsArtProducts;

interface SetPaletteInterface
{
    public function createPalette(PaintsArtProducts $paintsArtProduct);
}

