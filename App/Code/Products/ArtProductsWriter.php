<?php

namespace App\Code\Products;

abstract class ArtProductsWriter
{
    protected $products = array();

    public function addArtProduct(ArtProducts $artProduct)
    {
        $this->products[] = $artProduct;
    }

    abstract public function show();

}

