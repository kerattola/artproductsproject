<?php

namespace App\Code\Products;

class ArtShopList extends ArtProductsWriter
{
    public function show()
    {
        $list = "Art Products:\n";
        foreach ($this->products as $artProduct) {
            $list .= $artProduct->getProductInfo() . "\n";
        }
        echo $list;
    }

}

