<?php

namespace App\Code\Products;

use App\Code\Api\SetPaletteInterface;

class PaletteProducts implements SetPaletteInterface
{
    protected $palette = array();

    public function createPalette(PaintsArtProducts $paintsArtProduct)
    {
        $this->palette[] = $paintsArtProduct;
    }

    public function paletteInfo()
    {
        $info = "Palette Colours:\n";
        foreach ($this->palette as $paint) {
            $info .= $paint->getColour() . "\n";
        }
        echo $info;
    }

}