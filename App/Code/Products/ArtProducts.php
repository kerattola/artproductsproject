<?php
namespace App\Code\Products;

class ArtProducts
{
    private $title;
    private $price;
    private $brand;
    private $discount = 0;

    public function __construct($title, $price, $brand)
    {
        $this->title = $title;
        $this->price = $price;
        $this->brand = $brand;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPrice()
    {
        return $this->price - $this->discount;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getProductInfo()
    {
        $inf = "{$this->title}, Price: " . $this->getPrice() . ", Brand: {$this->brand}";
        return $inf;
    }

}

