<?php

namespace App\Code\Products;

class PaperArtProducts extends ArtProducts
{
    private $type;
    private $format;
    private $colour = "white";

    public function __construct($title, $price, $brand, $type, $format)
    {
        parent::__construct($title, $price, $brand);
        $this->type = $type;
        $this->format = $format;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function setColour($colour)
    {
        $this->colour = $colour;
    }

    public function getColour()
    {
        return $this->colour;
    }

    public function getProductInfo()
    {
        $inf = parent::getProductInfo();
        $inf .= ", Type: {$this->type}, Format: {$this->format}, Colour: {$this->colour}";
        return $inf;
    }
}

