<?php

namespace App\Code\Products;

class PaintsArtProducts extends ArtProducts
{
    private $type;
    private $volume;
    private $colour;

    public function __construct($title, $price, $brand, $type, $volume, $colour)
    {
        parent::__construct($title, $price, $brand);
        $this->type = $type;
        $this->volume = $volume;
        $this->colour = $colour;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getVolume()
    {
        return $this->volume;
    }

    public function getColour()
    {
        return $this->colour;
    }

    public function getProductInfo()
    {
        $inf = parent::getProductInfo();
        $inf .= ", Type: {$this->type}, Volume: {$this->volume}, Colour: {$this->colour}";
        return $inf;
    }
}

