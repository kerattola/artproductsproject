<?php

include('App/Autoload.php');

$art_prod1 = new App\Code\Products\ArtProducts("Sketchbook", 250, "Fabriano");
$art_prod2 = new App\Code\Products\ArtProducts("Brash", 150, "Rosa");

//abstract class
$my_list = new App\Code\Products\ArtShopList();
$my_list->addArtProduct($art_prod1);
$my_list->addArtProduct($art_prod2);
$my_list->show();

//interface
$paint1 = new App\Code\Products\PaintsArtProducts("Shine paint", 400, "Rosa", "Watercolour", 100, "Black");
$paint2 = new App\Code\Products\PaintsArtProducts("Matt paint", 400, "Rosa", "Acrylic", 200, "Red");

$palitra = new \App\Code\Products\PaletteProducts();
$palitra->createPalette($paint1);
$palitra->createPalette($paint2);
$palitra->paletteInfo();

$prod3 = new App\Code\Products\PaperArtProducts("Watercolour Paper", 280, "Carson", "Watercolour", "A4");

echo $prod3->getProductInfo() . "\n";
$prod3->setDiscount(50);
echo $prod3->getProductInfo();

